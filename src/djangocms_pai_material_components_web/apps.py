from django.apps import AppConfig


class DjangocmsPaiMaterialComponentsWebConfig(AppConfig):
    name = 'djangocms_pai_material_components_web'

